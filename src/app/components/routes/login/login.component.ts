import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  login: FormGroup = this.fb.group({
    email: [, [Validators.required, Validators.email]],
    password: [, [Validators.required, Validators.minLength(6)]]
  });

  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  campoEsValido(campo: string) {
    return (
      this.login.controls[campo].errors &&
      this.login.controls[campo].touched
    );
  }

  public ingresar() {
    if (this.login.invalid)
      return;
    const { email, password } = this.login.controls;
    this.authService.login(email.value, password.value).then(response => {
      if (!response)
        return
      console.log("Se Resgistro: ", response);
      this.router.navigate(['/home'])
    }
    )
  }
  public ingresarGoogle() {
    console.log("Ingresar");

    this.authService.loginWithGoogle().then(response => {
      if (!response)
        return
        console.log(response)
      localStorage.setItem('usuario', JSON.stringify(response.user))
      console.log("Se Resgistro: ", response);
      this.router.navigate(['/home'])
    }
    )
  }

}
