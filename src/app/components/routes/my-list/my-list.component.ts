import { Component, OnInit } from '@angular/core';
import { MovieSerie } from 'src/app/interfaces/movie-serie';
import { MovieService } from 'src/app/services/movie.service';

@Component({
  selector: 'app-my-list',
  templateUrl: './my-list.component.html',
  styleUrls: ['./my-list.component.css']
})
export class MyListComponent implements OnInit {
  public filter: string = 'Todos';
  user: any;
  movies: MovieSerie[] = [];

  constructor(
    private _movieService: MovieService
  ) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('usuario'));
    this.getMovies();
  }

  getMovies() {
    this._movieService.getList(this.user.uid).subscribe(
      response => {
        this.movies = [];
        console.log("esto es response",response)
        response.forEach((element:any) => {
          console.log(element.payload.doc.id)
          console.log(element.payload.doc.data())
          this.movies.push({
            idGlobal: element.payload.doc.id,
            ...element.payload.doc.data(),
          })
        })
        console.log("esta es la petición de movies", this.movies)
      },
      error => {
        console.log("falló la petición de movies", error)
      }
    )
  }

  deleteItem(id:string) {
    this._movieService.deleteItem(this.user.uid, id).then( () => {
      console.log('se eliminó correctamente')
    }).catch( error => {
      console.log(error)
    })
  }
  

}
