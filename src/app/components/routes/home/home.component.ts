import { Component, OnInit } from '@angular/core';
import { MovieSerie, MovieSerieBase } from '../../../interfaces/movie-serie';
import { MovieService } from '../../../services/movie.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public filter: string = 'Todos';

  public arrayData: MovieSerie[] = [];

  public search: string = '';
  public searchMovie: MovieSerie[] = [];

  constructor(private movieService: MovieService) { }

  ngOnInit(): void {
    this.movieService.getTrading().subscribe((response: any) => {
      response.results.forEach( m => {
        m.poster_path = `https://image.tmdb.org/t/p/original/${m.poster_path}`       
      });
      this.searchMovie = this.arrayData = response.results;
    });
    window.scroll({ top: 0, left: 0, behavior: 'smooth' });
  }
  public SetFilter(value: string) {
    this.filter = value;
  }

  public Search() {
    this.searchMovie = [];
    this.arrayData.forEach(data => {
      if (data.title?.toLowerCase().includes(this.search.toLowerCase()) || data.name?.toLowerCase().includes(this.search.toLowerCase()))
        this.searchMovie.push(data);
    });
    // debugger
    if (!this.search)
      this.searchMovie = this.arrayData;
  }

  public DataCount(): number {
    if (this.filter == 'Todos') {
      return this.searchMovie.length;
    } else {
      let count = 0;
      this.searchMovie.forEach(a => {
        if (a.media_type == this.filter)
          count++;
      });
      return count;
    }
  }

  public addList(item: MovieSerieBase) {
    let user = JSON.parse(localStorage.getItem('usuario'))
    this.movieService.addItem(user.uid, item).then( () => {
      console.log('se agregó el item al usuario')
    }).catch(error => {
      console.log(error);
    })
  }
}
