import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore'
import { MovieSerieBase, MovieSerieUser } from '../interfaces/movie-serie';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class MovieService {

  private baseUrl = 'https://api.themoviedb.org/3';

  private header = new HttpHeaders()
    .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI0N2FjOTlkMDQ1MjY1ZDZiODQzZWU3ZTQxN2Y0ODE4ZiIsInN1YiI6IjYyMTUyOTBmMzIzZWJhMDAxZWI3Njc1YyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.05CExT0KbAC1DiBGzAmqFW_CrYVgk4EY-HVfpsl7v6w')
    .set('Content-Type', 'application/json;charset=utf-8')

  constructor(
    private http: HttpClient,
    private firestore: AngularFirestore
  ) { }

  addUser(userId: MovieSerieUser): Promise <any> {
    return this.firestore.collection('usuarios').add(userId)
  }

  addItem(userId: MovieSerieUser, item: MovieSerieBase): Promise <any> {
    return this.firestore.collection('usuarios').doc(`${userId}`).collection('movies').add(item)
  }

  getList(userId: MovieSerieUser): Observable<any> {
    return this.firestore.collection('usuarios').doc(`${userId}`).collection('movies').snapshotChanges()
  }

  deleteItem(idUser:string, id: string): Promise<any> {
    return this.firestore.collection(`usuarios/${idUser}/movies`).doc(id).delete();
  }


  public getTrading() {
    return this.http.get(`${this.baseUrl}/trending/all/week`, { headers: this.header });
  }

  public getMovies({
    language = 'es',
    page = 1,
    region = null
  } = {}) {
    let params = new URLSearchParams([]);
    if (language) params.append("language", language);
    if (page) params.append("page", page.toString());
    if (region) params.append("region", region);
    return this.http.get(`${this.baseUrl}/movie/popular?${params.toString()}`, { headers: this.header });
  }

  public getSeries({
    language = 'es',
    page = 1,
    region = null
  } = {}) {
    let params = new URLSearchParams([]);
    if (language) params.append("language", language);
    if (page) params.append("page", page.toString());
    if (region) params.append("region", region);
    return this.http.get(`${this.baseUrl}/tv/popular?${params.toString()}`, { headers: this.header });
  }

}
