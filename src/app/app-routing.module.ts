import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/routes/home/home.component';
import { MoviesComponent } from './components/routes/movies/movies.component';
import { SeriesComponent } from './components/routes/series/series.component';
import { LoginComponent } from './components/routes/login/login.component';
import { MyListComponent } from './components/routes/my-list/my-list.component';


const routes: Routes = [
  {
    path: 'home',
    pathMatch: "full",
    component: HomeComponent,
  },
  {
    path: 'movies',
    pathMatch: "full",
    component: MoviesComponent,
  },
  {
    path: 'series',
    pathMatch: "full",
    component: SeriesComponent
  },
  {
    path: 'login',
    pathMatch: "full",
    component: LoginComponent
  },
  {
    path: 'my-list',
    pathMatch: "full",
    component: MyListComponent
  },
  {
    path: "",
    pathMatch: "full",
    redirectTo: "home"
  },
  {
    path: "**",
    redirectTo: "home"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
